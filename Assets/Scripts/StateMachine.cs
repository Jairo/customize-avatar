﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class StateMachine : MonoBehaviour
{
    public GameObject[] states;

    Animator _animator;

    // Use this for initialization
    void Awake()
    {
        _animator = GetComponent<Animator>();
        foreach (GameObject step in states) step.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

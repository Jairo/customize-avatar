﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour
{
    public SkinnedMeshRenderer baseMesh;
    public GameObject headDummy, backDummy, leftHandDummy, rightHandDummy;

    string gender;
    string className;

    Animator _animator;

    public enum ACCESSORIES_LOCATION
    {
        HEAD,
        BACK,
        MAIN
    }

    public enum WEAPONS_LOCATION
    {
        LEFT_HAND,
        RIGHT_HAND
    }

    public enum EMOTIONS
    {
        CRY
    }

	// Use this for initialization
	void Start ()
    {
        _animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("AvatarController::Update - QUIT APPLICATION");
            Application.Quit();
        }
    }

    Material GetResource (string resourceName)
    {
        Material newMaterial = Resources.Load(resourceName, typeof(Material)) as Material;
        if (newMaterial == null)
            Debug.Log("AvatarController::GetResource - Error loading asset at: Assets/Resources/" + resourceName);

        return newMaterial;
    }

    GameObject GetParent(ACCESSORIES_LOCATION location)
    {
        GameObject parent = null;
        switch (location)
        {
            case ACCESSORIES_LOCATION.BACK:
                parent = backDummy;
                break;
            case ACCESSORIES_LOCATION.HEAD:
                parent = headDummy;
                break;
            case ACCESSORIES_LOCATION.MAIN:
                parent = gameObject;
                break;
        }

        return parent;
    }

    GameObject GetParent(WEAPONS_LOCATION location)
    {
        GameObject parent = null;
        switch (location)
        {
            case WEAPONS_LOCATION.LEFT_HAND:
                parent = leftHandDummy;
                break;

            case WEAPONS_LOCATION.RIGHT_HAND:
                parent = rightHandDummy;
                break;

        }

        return parent;
    }

    void RemoveAllObjectsWithTag(GameObject baseObject, string tag)
    {
        foreach (Transform child in baseObject.transform)
        {
            if (child.gameObject.CompareTag(tag))
                Destroy(child.gameObject);
        }
    }

    void AttachAccessory(string filename, ACCESSORIES_LOCATION location)
    {
        UnityEngine.Object resource = Resources.Load(filename, typeof(GameObject));
        if (resource != null)
        {
            GameObject parent = GetParent(location);
            RemoveAllObjectsWithTag(parent, "ACCESSORY");

            GameObject newAccessory = InstantiateObject(parent, resource);
            newAccessory.tag = "ACCESSORY";
        }
        else
        {
            Debug.Log("AvatarController::AttachAccessory - Error loading asset at: Assets/Resources/" + filename);
        }
    }

    void AttachWeapon(string filename, WEAPONS_LOCATION location)
    {
        UnityEngine.Object resource = Resources.Load(filename, typeof(GameObject));
        if (resource != null)
        {
            GameObject parent = GetParent(location);
            RemoveAllObjectsWithTag(parent, "WEAPON");

            GameObject newWeapon = InstantiateObject(parent, resource);
            newWeapon.tag = "WEAPON";
        }
        else
        {
            Debug.Log("AvatarController::AttachWeapon - Error loading asset at: Assets/Resources/" + filename);
        }
    }

    private GameObject InstantiateObject(GameObject parent, UnityEngine.Object resource)
    {
        if (parent != null)
        {
            GameObject accesory = Instantiate(resource, parent.transform) as GameObject;
            return accesory;
        }

        Debug.Log("AvatarController::AttachAccesories - Select a valid location for the accesory");
        return null;
    }

    void PlayEmotion(string filename)
    {
        UnityEngine.Object faceResource = Resources.Load(filename, typeof(GameObject));
        if (faceResource != null)
        {
            RemoveAllObjectsWithTag(headDummy, "FACE");
            GameObject newFace = InstantiateObject(headDummy, faceResource);
            newFace.tag = "FACE";
        }
        else
        {
            Debug.Log("AvatarController::SelectHair - Error loading asset at: Assets/Resources/" + filename);
        }
    }

    // Public Methods

    public void SelectGender(string gender)
    {
        Material newMaterial = GetResource(gender + " White Naked");
        if (newMaterial != null)
        {
            baseMesh.material = newMaterial;
            this.gender = gender;
        }
    }

    public void SelectClass(string className)
    {
        if (className == "Wizard" && gender == "Female") className = "Sorceress";

        for (int i = 1; i <= 6; i++)
        {
            Material newMaterial = GetResource(gender + " White " + className + " " + i.ToString("D2") + " White");
            if (newMaterial != null)
            {
                baseMesh.material = newMaterial;
                this.className = className;
                return;
            }
        }
    }

    public void SelectHair(int hairStyle)
    {
        string resourceName = "Hair " + gender + " " + hairStyle.ToString("D2") + " Black";
        UnityEngine.Object hairResource = Resources.Load(resourceName, typeof(GameObject));
        if (hairResource != null)
        {
            foreach (Transform child in headDummy.transform) Destroy(child.gameObject);
            GameObject hair = Instantiate(hairResource, headDummy.transform) as GameObject;
        }
        else
        {
            Debug.Log("AvatarController::SelectHair - Error loading asset at: Assets/Resources/" + resourceName);
        }
    }

    public void AttachToHead(string filename)
    {
        AttachAccessory(filename, ACCESSORIES_LOCATION.HEAD);
    }

    public void AttachToBack(string filename)
    {
        AttachAccessory(filename, ACCESSORIES_LOCATION.BACK);
    }

    public void AttachToMain(string filename)
    {
        AttachAccessory(filename, ACCESSORIES_LOCATION.MAIN);
    }

    public void AttachToLeftHand(string filename)
    {
        AttachWeapon(filename, WEAPONS_LOCATION.LEFT_HAND);
    }

    public void AttachToRigthHand(string filename)
    {
        AttachWeapon(filename, WEAPONS_LOCATION.RIGHT_HAND);
    }

    public void DetachAllWeapons()
    {
        GameObject parent;

        parent = GetParent(WEAPONS_LOCATION.LEFT_HAND);
        RemoveAllObjectsWithTag(parent, "WEAPON");

        parent = GetParent(WEAPONS_LOCATION.RIGHT_HAND);
        RemoveAllObjectsWithTag(parent, "WEAPON");
    }

    public void Cry()
    {
        string resourceFilename = (gender == "Female") ? "Face White 21" : "Face White 01";
        PlayEmotion(resourceFilename);

        _animator.SetBool("Clapping", false);
        _animator.SetBool("Wave Hand", false);
        _animator.SetBool("Crying", true);
    }

    public void Applause()
    {
        string resourceFilename = (gender == "Female") ? "Face White 24" : "Face White 03";
        PlayEmotion(resourceFilename);

        _animator.SetBool("Crying", false);
        _animator.SetBool("Wave Hand", false);
        _animator.SetBool("Clapping", true);
    }

    public void Salute()
    {
        string resourceFilename = (gender == "Female") ? "Face White 30" : "Face White 10";
        PlayEmotion(resourceFilename);

        _animator.SetBool("Crying", false);
        _animator.SetBool("Clapping", false);
        _animator.SetBool("Wave Hand", true);
    }
}
